# 風電系統運轉檢測實務

## Chapter 1 Example GUN C/C++ on Windows

* 1-1 GNU C/C++ Requirements
  * [Install Mingw-w64 with MSYS2](chapter_01/install_msys2.md)
  * [Install Open-JDK](chapter_01/install_java.md)
  * [Install Eclipse](chapter_01/install_eclipse.md)
  * [Install git for windows](chapter_01/install_git.md)
* 1-2 C++ HelloWorld
  * Create Project
  * [C++ HelloWorld](chapter_01/example_helloworld.md)
  * Build
  * Run
  * Debug
* 1-3 Git Usage Guide
  * Create repository
  * Setting Local Env
  * Initial local Git
  * commit
  * push and pull
  * branch
  * clone

## Chapter 2 Motor and Load Control using Modbus

* 2-1 Modbus C Programming Requirements
  * [Build Library libmodbus](chapter_02/build_modbus.md)
* 2-2 Modbus Example
  * [Serial Port](chapter_02/device_serialport.md)
  * [Example Modbus](chapter_02/example_modbus.md)
* 2-3 Control Motor by Inverter
  * [Delta Electronics VFD002S23A](chapter_02/VFD-S_UM_TC_20160412.pdf)
* 2-4 Control Load by DC Power Supply
  * [DPS3005 PDF](chapter_02/DPS3005%20CNC%20Communication%20%20Protocol%20V1.2.pdf)

## Chapter 3 Data Capture using ADC on Embedded System

* 3-1 Embedded System Development Requirements
  * [Install Virtual Box](chapter_03/install_virtualbox.md)
  * [Install XShell]()
  * [Install XFtp](chapter_03/install_xftp.md)
* 3-2 Embedded Development Environment
  * [Install Ubuntu](chapter_03/install_ubuntu.md)
  * [Ubuntu Settings](chapter_03/ubuntu_setting.md)
  * [Install TI Processor SDK for AM335x Sitara Processor](chapter_03/install_sdk.md)
  * [Install TI Code Composer Studio](chapter_03/install_ccs.md)
  * [Build SDK](chapter_03/build_sdk.md)
  * [Setting Internet for Embedded](chapter_03/embedded_internet.md)
* Example
  * [VM](https://140.118.172.165:5001/fbdownload/Ubuntu_18.04_TISDK_AM3358_v3.ova?_sid=%22m.ctXdEdsHbgkD2LCN00910%22&mode=open&dlink=%222f32305f4c696e75782f5562756e74755f31382e30345f544953444b5f414d333335385f76332e6f7661%22&stdhtml=true&SynoToken=5.547qMsKv.E.)
  * [Boot Embedded System](chapter_03/embedded_boot.md)
  * [HelloWorld on Embedded](chapter_03/example_crosscompile.md)
  * [ADC Capture](chapter_03/example_ads8556.md)

## Chapter 4 Electronic Analysis

* Dependent Requirements
  * [Install Mingw-w64 with MSYS2](chapter_01/install_msys2.md)
  * [Install Open-JDK](chapter_01/install_java.md)
  * [Install Eclipse](chapter_01/install_eclipse.md)
* Example
  * [Math Root Mean Square](chapter_04/example_math.md)

## Chapter 5 Qt

* System Requirements
  * [Install Qt](chapter_05/install_qt.md)
* Example
  * [HelloWorld Qt](chapter_05/example_qt.md)
  * [HelloWorld Qt Chart]()

## Chapter 6 RESTful API

* Dependent Requirements
  * [Install Virtual Box](chapter_03/install_virtualbox.md)
  * [Install XShell]()
  * [Install XFtp](chapter_03/install_xftp.md)
  * Embedded Development Environment
    * [Install Ubuntu]()
    * [Ubuntu Settings](chapter_03/ubuntu_setting.md)
    * [Install TI Processor SDK for AM335x Sitara Processor](chapter_03/install_sdk.md)
    * [Install TI Code Composer Studio](chapter_03/install_ccs.md)
    * [Build SDK](chapter_03/build_sdk.md)
    * [Setting Internet for Embedded](chapter_03/embedded_internet.md)
* System Requirements
  * Server
    * [Library pistache]()
  * Client
    * [restclient-cpp]()
* Example
  * [ADC REST Service]()
  * [ADC Remote Control]()
  * [Qt ADC Remote monitoring]()

## Chapter 7 MQTT

* Dependent Requirements
  * [Install Virtual Box](chapter_03/install_virtualbox.md)
  * [Install XShell]()
  * [Install XFtp](chapter_03/install_xftp.md)
  * Embedded Development Environment
    * [Install Ubuntu]()
    * [Ubuntu Settings](chapter_03/ubuntu_setting.md)
    * [Install TI Processor SDK for AM335x Sitara Processor](chapter_03/install_sdk.md)
    * [Install TI Code Composer Studio](chapter_03/install_ccs.md)
    * [Build SDK](chapter_03/build_sdk.md)
    * [Setting Internet for Embedded](chapter_03/embedded_internet.md)
  * [Install Mingw-w64 with MSYS2](chapter_01/install_msys2.md)
  * [Install Open-JDK](chapter_01/install_java.md)
  * [Install Eclipse](chapter_01/install_eclipse.md)
* System Requirements
  * [Library Paho MQTT](chapter_07/build_mqtt.md)
  * [CentOS Server]()
    * [Open-Jdk for CentOS]()
    * [Apache ActiveMQ(MQTT Broker]()
* Example
  * [ADC MQTT Publish]()
  * [MQTT Subscribe]()

## Chapter 8 Apache Kafka

* Dependent Requirements
  * [CentOS Server]()
  * [Open-Jdk for CentOS]()
  * [Open-JDK for Windows]()
* System Requirements
  * [Apache Kafka]()
  * [NetBeans]()
* Example
  * [Kafka Connect - Source Connector]()
  * [Kafka Stream]()
  * [Kafka Connect - Sink Connector]()

## Chapter 9 Database Cassandra

* Dependent Requirements
  * [CentOS Server]()
  * [Open-Jdk for CentOS]()
  * [Open-JDK for Windows]()
  * [NetBeans]()
* System Requirements
  * [Apache Cassandra]()
* Example
  * [Table]()
  * [insert]()
  * [select]()

## Chapter 10 Business Intelligence Apache Superset

* Dependent Requirements
  * [CentOS Server]()
  * [Open-Jdk for CentOS]()
  * [Open-JDK for Windows]()
  * [NetBeans]()
* System Requirements
  * [Apache Superset]()
* Example
  * [Sample Chart]()

## Chapter 11 Machane Learning

* Dependent Requirements
  * [CentOS Server]()
  * [Open-Jdk for CentOS]()
  * [Open-JDK for Windows]()
  * [NetBeans]()
* System Requirements
  * [Apache Spark]()
* Example
  * [K-mean]()




