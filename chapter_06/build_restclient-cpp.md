C++ client for making HTTP/REST requests https://github.com/mrtazz/restclient-cpp
![](build_restclient-cpp.png/restclient-cpp_01.png)
開啟msys2 
``` 
pacman -S mingw-w64-x86_64-curl
```
```
cd workspace_msys2 
```
``` 
git clone https://github.com/mrtazz/restclient-cpp.git
```
![](build_restclient-cpp.png/restclient-cpp_02.png)
```
cd restclient-cpp/  
```
```
./autogen.sh
```

![](build_restclient-cpp.png/restclient-cpp_03.png)
```
./configure
```

![](build_restclient-cpp.png/restclient-cpp_04.png)
```
make install
```

![](build_restclient-cpp.png/restclient-cpp_05.png)  

結束安裝

![](build_restclient-cpp.png/restclient-cpp_06.png)

建立測試資料夾
```
mkdir workspace_test
```
進入資料夾
```
cd workspace_test
```
![](build_restclient-cpp.png/restclient-cpp_07.png)


建立專案資料夾

```
mkdir rest_example
```
```
cd rest_example
```

 

[第二步](https://gitlab.com/108_2_WPSOD/example/08_restapi_client/-/blob/e0f2a55e623b2e2b7a36a32691fe4931a54daa44/rest_example.cpp)

建立文件
```
vim rest_example.cpp
```

![](build_restclient-cpp.png/restclient-cpp_08.png)

程式碼如下圖所示


```
:wq         \\存檔離開
```

![](build_restclient-cpp.png/restclient-cpp_09.png)



編譯程式

```
g++ rest_example.cpp -o rest_example -lrestclient-cpp -lcurl
```

![](build_restclient-cpp.png/restclient-cpp_10.png)

執行程式
```
./rest_example.exe
```

![](build_restclient-cpp.png/restclient-cpp_11.png)

[第三步](https://github.com/Tencent/rapidjson/)

```
git clone https://github.com/Tencent/rapidjson.git
```

```
g++ -I rapidjson/include rest_example.cpp -o rest_example -lrestclient-cpp -lcurl
```

```
./rest_example.exe
```

