# Install JDK

至以下連結下載OpenJDK:

https://openjdk.java.net/

點"jdk.java.net/13"

![](install_java.png/java_01.png)

點"jdk.java.net"

![](install_java.png/java_02.png)

點Windows/x64 zip(sha256)

![](install_java.png/java_03.png)

檔案下載中

![](install_java.png/java_04.png)

點選檔案右鍵 > 在資料夾中顯示

![](install_java.png/java_05.png)

資料夾位置

![](install_java.png/java_06.png)

右鍵 > 解壓縮全部

![](install_java.png/java_07.png)

解壓縮，並設定路徑

![](install_java.png/java_08.png)

繼續

![](install_java.png/java_09.png)

program files > jdk-13.0.2 > bin 

![](install_java.png/java_10.png)
![](install_java.png/java_11.png)
![](install_java.png/java_12.png)

右鍵複製連結 : C:\\Program Files\jdk-13.0.2\bin

![](install_java.png/java_13.png)

本機 > 右鍵內容

![](install_java.png/java_14.png)

進階系統設定

![](install_java.png/java_15.png)

環境變數

![](install_java.png/java_16.png)

系統變數 : 新增

![](install_java.png/java_17.png)

變數名稱: JAVA_HOME

變數值: C:\Program Files\jdk-13.0.2

確定

![](install_java.png/java_18.png)

系統變數 : 選Path > 編輯

![](install_java.png/java_19.png)

新增

![](install_java.png/java_20.png)

輸入: %JAVA_HOME%\bin > 確定

![](install_java.png/java_21.png)

確定

![](install_java.png/java_22.png)

確定

![](install_java.png/java_23.png)

開啟執行視窗cmd

![](install_java.png/java_24.png)

跳出執行視窗

![](install_java.png/java_25.png)

查看java版本

```batchfile
java -version
```

![](install_java.png/java_26.png)

查看javac版本

```batchfile
javac -version
```

![](install_java.png/java_27.png)
