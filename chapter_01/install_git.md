# Install Git

到Git官網 https://git-scm.com/

![](install_git.png/git_01.png)

下載Git

![](install_git.png/git_02.png)

Next

![](install_git.png/git_03.png)

Next

![](install_git.png/git_04.png)

Next

![](install_git.png/git_05.png)

Next

![](install_git.png/git_06.png)

Next

![](install_git.png/git_07.png)

選第2個Git from the command line and also from 3rd-party software >　Next

![](install_git.png/git_08.png)

選第1個Use the OpenSSL library > Next 

![](install_git.png/git_09.png)

選第1個Checkout Windows-Style, commit Unix-style line endings > Next

![](install_git.png/git_10.png)

選第1個Use MinTTY (the default terminal of MSY52) > Next

![](install_git.png/git_11.png)

選第1、2個Enable file system caching、Enable Git Credential Manager > Next

![](install_git.png/git_12.png)

下載中

![](install_git.png/git_13.png)

選第2個View Release Notes > Next

![](install_git.png/git_14.png)

Relase Notes

![](install_git.png/git_15.png)
