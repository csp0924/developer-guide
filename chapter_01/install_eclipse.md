# Install Eclipse IDE

至以下連結下載Eclipse IDE:

https://www.eclipse.org/

點Download

![](install_eclipse.png/eclipse_01.png)

點Download 64bit

![](install_eclipse.png/eclipse_02.png)

點Download

![](install_eclipse.png/eclipse_03.png)

下載中...

![](install_eclipse.png/eclipse_04.png)

點"是"

![](install_eclipse.png/eclipse_05.png)

到Program Files > jdk-13.0.2 > bin > 開啟javaw.exe

![](install_eclipse.png/eclipse_06.png)

javaw.exe開啟畫面

![](install_eclipse.png/eclipse_07.png)

選Eclipse IED for C/C++ Developers

![](install_eclipse.png/eclipse_08.png)

點Install

![](install_eclipse.png/eclipse_09.png)

點Accpet Now

![](install_eclipse.png/eclipse_10.png)

安裝中...

![](install_eclipse.png/eclipse_11.png)

點Accept

![](install_eclipse.png/eclipse_12.png)

點Select All > Accept selected

![](install_eclipse.png/eclipse_13.png)

點LAUNCH

![](install_eclipse.png/eclipse_14.png)



![](install_eclipse.png/eclipse_15.png)

Workspace為預設的位置 > Lauch

![](install_eclipse.png/eclipse_16.png)

開啟eclipse IDE中...

![](install_eclipse.png/eclipse_17.png)

來到eclipse IDE畫面

![](install_eclipse.png/eclipse_18.png)
