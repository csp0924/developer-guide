# Example C Hello World


開啟Eclipse

File > New > C/C++ Projet

![](example_helloworld.png/helloworld_01.png)


All > C Managed Build

![](example_helloworld.png/helloworld_02.png)

輸入Project Name

Project type : Executable > Hello World ANSI Project

Toolchains : MinGW GCC

![](example_helloworld.png/helloworld_03.png)

Finish

![](example_helloworld.png/helloworld_04.png)

勾選Debug和Release > Finish

![](example_helloworld.png/helloworld_05.png)

進入Console版面

![](example_helloworld.png/helloworld_06.png)

點左上角槌子圖案後，下方console面板顯示 CDT Build Console [helloworld].......

![](example_helloworld.png/helloworld_07.png)

點左上角播放圖案後，下方console面板顯示 <terminated> (exit value:0) helloworld.exe......，並顯示訊息

![](example_helloworld.png/helloworld_08.png)

開啟Git Bash

![](example_helloworld.png/helloworld_09.png)

跳出執行視窗

![](example_helloworld.png/helloworld_10.png)

設定使用者名稱及電子郵件

```shell
$ git config --global user.name "使用者名稱"

$ git config --global user.email "電子郵件"
```

![](example_helloworld.png/helloworld_11.png)

進入至eclipse-worksapce資料夾 > 顯示所有檔案 > 進入helloworld資料夾 > 顯示所有檔案

```shell
$ cd eclipse-workspace/

$ ls -l

$ cd helloword/

$ ls -l
``` 

![](example_helloworld.png/helloworld_12.png)

初始化這個資料夾，讓 Git 開始對這個資料夾進行版本控制

```shell
$ git init  
```

看到資料夾內有.git，代表初始化成功

![](example_helloworld.png/helloworld_13.png)

顯示所有檔名，包含"."開頭的隱藏檔

```shell
$ ls -la
```

新增遠端版本庫

```shell
$ git remote add <簡稱> <url> 
```

![](example_helloworld.png/helloworld_14.png)

查詢現在這個資料夾的「狀態」

```shell
$ git status
```

目前狀態: 偵測到有5個檔案尚未被加到 Git 版控系統裡(紅字)，還沒開始正式被 Git「追蹤」

![](example_helloworld.png/helloworld_15.png)

將這三個檔案".cproject" ".settings/" "Debug/" 附加到 .gitignore檔案裡，目的是為了讓Git忽略這三個檔案，不上傳到Git上

```shell
$ echo 檔案名稱 >> .gitignore
```

```shell
$ echo .cproject >> .gitignore
$ echo .settings/ >> .gitignore
$ echo Debug/ >> .gitignore
```

![](example_helloworld.png/helloworld_16.png)

顯示.gitignore檔案裡的內容

```shell
$ cat .gitignore
```

![](example_helloworld.png/helloworld_17.png)

再次查詢現在這個資料夾的「狀態」

目前狀態: 偵測到有3個為檔案尚未被加到 Git 版控系統裡，還沒開始正式被 Git「追蹤」，而原本的".cproject" ".settings" "Debug"則被Git忽略



![](example_helloworld.png/helloworld_18.png)

將全部檔案加入索引

```shell
$ git add .
```

![](example_helloworld.png/helloworld_19.png)

備註message並提交到git

```shell
$ git commit -m "<commit message>"
```

![](example_helloworld.png/helloworld_20.png)

檢視提交的歷史記錄

```shell
$ git log
```

![](example_helloworld.png/helloworld_21.png)

上傳到remote (ex. gitlab, github, bitbucket )

```shell
$ git push origin master
```

![](example_helloworld.png/helloworld_22.png)

登入Gitlab

![](example_helloworld.png/helloworld_23.png)

Git push的結果:上傳完成

![](example_helloworld.png/helloworld_24.png)

Gitlab呈現結果

![](example_helloworld.png/helloworld_25.png)

