# Install MSYS2

MSYS2是Windows的軟件發行版和構建平台
它的核心是基於現代Cygwin（POSIX兼容層）和MinGW-w64的MSYS的獨立重寫，目的是更好地與本機Windows軟件互操作。它提供了一個bash shell，Autotools，版本控制系統等，用於使用MinGW-w64工具鏈構建本機Windows應用程序。

它具有軟件包管理系統Pacman，可輕鬆安裝軟件包。它帶來了許多強大的功能，例如依賴關係解析和簡單的完整系統升級，以及直接的軟件包構建。

至MSYS2官網: https://www.msys2.org/

下載並運行安裝程序，64位元版本 msys2-x86_64.exe

![](install_msys2.png/msys2_01.png)

執行安裝程式

![](install_msys2.png/msys2_02.png)

點擊“下一步”

![](install_msys2.png/msys2_03.png)

輸入安裝文件夾（ASCII，無重音，空格或符號鏈接，短路徑）

![](install_msys2.png/msys2_04.png)

安裝中

![](install_msys2.png/msys2_05.png)

勾選立即運行MSYS2

![](install_msys2.png/msys2_06.png)

開啟Msys64執行視窗

![](install_msys2.png/msys2_07.png)

使用以下命令更新軟件包數據庫和核心系統軟件包：

```sh
$ pacman -Syu
```

![](install_msys2.png/msys2_08.png)

進行安裝嗎? y

![](install_msys2.png/msys2_09.png)

如果需要，請關閉MSYS2，

![](install_msys2.png/msys2_10.png)

OK

![](install_msys2.png/msys2_11.png)

然後從“開始”選單再次開啟MSYS2 MSYS(MSYS2 64bit)

![](install_msys2.png/msys2_12.png)

MSYS2

![](install_msys2.png/msys2_13.png)

使用以下命令更新其餘部分：

```sh
$ pacman -Su
```

![](install_msys2.png/msys2_14.png)

進行安裝嗎? y

![](install_msys2.png/msys2_15.png)

正在更新...

![](install_msys2.png/msys2_16.png)

安裝vim git

```bash
$ pacman -S vim git
```

進行安裝嗎? y

![](install_msys2.png/msys2_17.png)

正在安裝...

![](install_msys2.png/msys2_18.png)

MSYS2的安裝程式並沒有自帶MinGW，如果要使用MinGW的話，下指令:

```bash
$ pacman -S mingw-w64-x86_64-toolchain
```

![](install_msys2.png/msys2_19.png)

直接Enter

![](install_msys2.png/msys2_20.png)

進行安裝嗎? y

![](install_msys2.png/msys2_21.png)

正在安裝...

![](install_msys2.png/msys2_22.png)

安裝Base-devel

```bash
$ pacman -S base-devel
```

![](install_msys2.png/msys2_23.png)



![](install_msys2.png/msys2_24.png)

進行安裝嗎? y

![](install_msys2.png/msys2_25.png)

正在安裝...

![](install_msys2.png/msys2_26.png)

本機 > 右鍵內容

![](install_msys2.png/msys2_27.png)

進階系統設定

![](install_msys2.png/msys2_28.png)

環境變數

![](install_msys2.png/msys2_29.png)

系統變數： Path > 編輯

![](install_msys2.png/msys2_30.png)

新增2項

> C:msys64\usr\bin

> C:msys64\mingw64\x86_64-w64-mingw32\bin

然後確定

![](install_msys2.png/msys2_31.png)

確定

![](install_msys2.png/msys2_32.png)

確定

![](install_msys2.png/msys2_33.png)
