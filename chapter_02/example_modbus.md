# Modbus example

開啟Eclipse-workspace > File >　New > C/C++ Project

![](example_modbus.png/modbus_01.png)

All > C Managed Build > Next

![](example_modbus.png/modbus_02.png)

Project Name : 02_modbus

Project Type : Hello World ANSI Project

Toolchains: MinGW GCC

Finish

![](example_modbus.png/modbus_03.png)

Basic Settings

![](example_modbus.png/modbus_04.png)

Author : 輸入名字 > Finish

![](example_modbus.png/modbus_05.png)

勾選Debug和Release > Finish

![](example_modbus.png/modbus_06.png)

02_modbus按右鍵 > Build Project

![](example_modbus.png/modbus_07.png)

Console面板顯示如下

![](example_modbus.png/modbus_08.png)

02_modbus按右鍵 > Run as > 2LocalC/C++ Application

![](example_modbus.png/modbus_09.png)

Console面板顯示如下

![](example_modbus.png/modbus_10.png)

modbus範例程式

![](example_modbus.png/modbus_11.png)

編譯之後發現有錯(紅色地方)

![](example_modbus.png/modbus_12.png)

解決方式 : 加入#include <modbus/modbus.h> (第14 line)

![](example_modbus.png/modbus_13.png)

發現另一個錯誤(紅色地方)

![](example_modbus.png/modbus_14.png)

解決方式 : 02_modbus按右鍵 > Properties

![](example_modbus.png/modbus_15.png)

C/C++ Build > Settings > Tool Settings >　GCC C Compiler > Includes > Apply and Close

![](example_modbus.png/modbus_16.png)

Directory: C:\msys64\FLS5110\Workspace_msys2\libmodbus\src > OK

![](example_modbus.png/modbus_17.png)

Include paths(-l)地方顯示路徑 > Apply and Close

![](example_modbus.png/modbus_18.png)

Yes

![](example_modbus.png/modbus_19.png)

發現錯誤(紅色地方)

![](example_modbus.png/modbus_20.png)

解決方式 : C/C++ Build > Settings > Tool Settings > MinGW Linker > Libraries > Apply and Close

![](example_modbus.png/modbus_21.png)

輸入libmodbus-5 > OK

![](example_modbus.png/modbus_22.png)

Libraries(-l) : libmodubs-5

Library search path(-L) : "C\msys64\mingw64\bin" > Apply and Close

![](example_modbus.png/modbus_23.png)

Console面板顯示如下

![](example_modbus.png/modbus_24.png)

Console面板顯示如下

![](example_modbus.png/modbus_25.png)

上gitlab > 左方Detail > 按綠色按鈕New Project

![](example_modbus.png/modbus_26.png)

Gitlab建立專案，輸入Project name : 02_modbus > Create project

![](example_modbus.png/modbus_27.png)

專案建立完成

![](example_modbus.png/modbus_28.png)

開啟Git Bash

![](example_modbus.png/modbus_29.png)

切換至eclipse-workspace資料夾

```bash
$ cd eclipse-workspace\
```

顯示所有檔案

```bash
$ ls -l
```

切換至 02_mobdus資料夾

```bash
$ cd 02_mobdus\
```

顯示所有檔案

```bash
$ ls -l
```

![](example_modbus.png/modbus_30.png)

複製連結git remote add origin https://gitlab.com/108_2_WPS0D/example/02_modbus.git ，待新增遠端版本庫用

![](example_modbus.png/modbus_31.png)

初始化這個資料夾，讓 Git 開始對這個資料夾進行版本控制

```bash
$ git init  
```

看到資料夾內有.git，代表初始化成功

新增遠端版本庫

```bash
$ git remote add origin https://gitlab.com/108_2_WPS0D/example/02_modbus.git
```

![](example_modbus.png/modbus_32.png)

查詢現在這個資料夾的「狀態」

```bash
$ git status
```

目前狀態: 偵測到有5個檔案尚未被加到 Git 版控系統裡，還沒開始正式被 Git「追蹤」

![](example_modbus.png/modbus_33.png)

將這三個檔案".cproject" ".settings/" "Debug/" 附加到 .gitignore檔案裡，目的是為了讓Git忽略這三個檔案

```bash
$ echo .cproject >> .gitigore

$ echo .settings/ >> .gitigore

$ echo Debug/ >> .gitigore
```

![](example_modbus.png/modbus_34.png)

再次查詢現在這個資料夾的「狀態」

目前狀態: 偵測到有3個為檔案尚未被加到 Git 版控系統裡，還沒開始正式被 Git「追蹤」，而原本的".cproject" ".settings" "Debug"則被Git忽略

![](example_modbus.png/modbus_35.png)

一口氣將全部檔案加入索引

```bash
$ git add .
```

![](example_modbus.png/modbus_36.png)

備註test modubs並提交到git

```bash
$ git commit -m "<test modubs>"
```

![](example_modbus.png/modbus_37.png)

上傳到remote (ex. gitlab, github, bitbucket )

```bash
$ git push origin master
```

![](example_modbus.png/modbus_38.png)

Git push的結果 : 上傳完成

![](example_modbus.png/modbus_39.png)

Gitlab呈現結果

![](example_modbus.png/modbus_40.png)


