# Build modbus

至libmodbus官網:　https://libmodbus.org/

![](build_modbus.png/libmodbus_01.png)

下載

![](build_modbus.png/libmodbus_02.png)

至https://github.com/stephane/libmodbus > 選按鈕「Clone or download」 複製網址

![](build_modbus.png/libmodbus_03.png)

開啟MSYS2 MiniGW 64-bit

![](build_modbus.png/libmodbus_04.png)

跳出執行視窗

![](build_modbus.png/libmodbus_05.png)

新增資料夾workspace_msys2

```bash
$ mkdir workspace_msys2
```

切換至資料夾workspace_msys2

```bash
$ cd workspace_msys2/
```

把libmodbus整個的專案內容複製一份到你的電腦裡

```bash
$ git clone https://github.com/stephane/libmodbus.git
```

顯示所有檔案

```bash
$ ls -l
```

切換至資料夾libmodbus

```bash
$ cd libmodbus/
```

![](build_modbus.png/libmodbus_06.png)

顯示所有檔案

```bash
$ ls -l
```

![](build_modbus.png/libmodbus_07.png)

列出標籤(版本)

```bash
$ git tag
```

![](build_modbus.png/libmodbus_08.png)

取出tag v3.1.6版

```bash
$ git checkout tags/v3.1.6
```

顯示分支列表，前面有 * 的代表現在的分支

```bash
$ git branch
```

![](build_modbus.png/libmodbus_09.png)

進行編譯: autogen.sh > configure > make

下autogen.sh指令，產生configure腳本

```bash
$./autogen.sh
```

![](build_modbus.png/libmodbus_10.png)

下configure指令，檢查系統配置

```bash
$./configure
```

![](build_modbus.png/libmodbus_11.png)

configure完的結果

![](build_modbus.png/libmodbus_12.png)

下make指令，進行代碼編譯

```bash
$ make
```

![](build_modbus.png/libmodbus_13.png)

make完的結果

![](build_modbus.png/libmodbus_14.png)

下make install指令，安裝編譯產生的檔案

```bash
$ make install
```

![](build_modbus.png/libmodbus_15.png)

make install完的結果

![](build_modbus.png/libmodbus_16.png)
