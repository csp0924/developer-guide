# Device serial port


開啟執行 > 輸入"compmgmt.msc"開啟電腦管理(也可直接於開始右鍵開啟裝置管理員)

![](device_serialport.png/serialport_01.png)

點選裝置管理員

![](device_serialport.png/serialport_02.png)

確認USE-SERIAL CH340的COM port number > 修改modbus的COM port number

![](device_serialport.png/serialport_03.png)
