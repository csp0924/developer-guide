# Job_upload.me


上傳作業流程

作業上傳前先確認專案是否建立，建立完後
開啟OPEN Tetminal或是git Bash

windows介面通常

```shell
$ cd eclipse-workspace/"專案名稱"/
```

以上是預設路徑

linux介面通常

```shell
$ cd workspace_v9/"專案名稱"/
```

以上是預設路徑
網頁開到專案上傳的地方

* 第一步 Git全局設置

```shell
$ git config --global user.name "學號"
$ git config --global user.email "信箱"
```

* 第二步 準備上傳

```shell
$ git init
$ git remote add origin "請輸入網頁上給的"
```
如果上面路徑有錯誤請打下面的指令並從第一步開始，如無誤慶跳過至第三步

```shell
$ rm -rf .git
```

* 第三步 新增不上傳物件

```shell
$ echo .settings/ >> .gitignore
$ echo Debug/ >> .gitignore
```

* 第四步 上傳

```shell
git add .
git commit -m "備註"
git push -u origin master
```


