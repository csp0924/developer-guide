
在CCS開啟新專案

![](example_ads8556.png/ads8556_01.png)

C++專案

![](example_ads8556.png/ads8556_02.png)

example_ads8556

![](example_ads8556.png/ads8556_03.png)

作者

![](example_ads8556.png/ads8556_04.png)

下一步

![](example_ads8556.png/ads8556_05.png)

下一步

![](example_ads8556.png/ads8556_06.png)

編譯專案

![](example_ads8556.png/ads8556_07.png)

查看是否成功

![](example_ads8556.png/ads8556_08.png)

改程式

![](example_ads8556.png/ads8556_09.png)

對專案右鍵->Debug As->Debug configurations

![](example_ads8556.png/ads8556_10.png)

c/c++的地方按下右鍵 new configuratio

![](example_ads8556.png/ads8556_11.png)

點擊畫面右邊中間 New 連線模式選SSH

![](example_ads8556.png/ads8556_12.png)

Host : "崁入式系統IP"  
Usrr : root  
finish

![](example_ads8556.png/ads8556_13.png)

apply

![](example_ads8556.png/ads8556_14.png)

yes

![](example_ads8556.png/ads8556_15.png)

起始路徑  /home/root  


![](example_ads8556.png/ads8556_16.png)

空白處右鍵新增資料夾  
workspace

![](example_ads8556.png/ads8556_17.png)

OK

![](example_ads8556.png/ads8556_18.png)

apply

![](example_ads8556.png/ads8556_19.png)

視窗右上 RUN -> load -> example_ads8556

![](example_ads8556.png/ads8556_20.png)

出警告

![](example_ads8556.png/ads8556_21.png)

專案按右鍵properties

![](example_ads8556.png/ads8556_22.png)

c/c++Build -> settings  
Cross G++ Linker -> libraries -> add

![](example_ads8556.png/ads8556_23.png)

輸入參數spsadc -> OK

![](example_ads8556.png/ads8556_24.png)

編譯 -> 編譯成功

![](example_ads8556.png/ads8556_25.png)

輸入程式

![](example_ads8556.png/ads8556_26.png)

到嵌入式執行 ./example_ads8556

![](example_ads8556.png/ads8556_27.png)

設定浮點數  
存檔

![](example_ads8556.png/ads8556_28.png)

到嵌入式執行 ./example_ads8556  
顯示檔案資料  cat data.csv |head

![](example_ads8556.png/ads8556_29.png)

開啟Xshell連線

![](example_ads8556.png/ads8556_30.png)

開起xftp

![](example_ads8556.png/ads8556_31.png)

進入workspace資料夾

![](example_ads8556.png/ads8556_32.png)

點兩下data.csv

![](example_ads8556.png/ads8556_33.png)



![](example_ads8556.png/ads8556_34.png)



![](example_ads8556.png/ads8556_35.png)
