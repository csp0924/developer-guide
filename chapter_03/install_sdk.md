
至以下連結下載Processor SDK:

http://www.ti.com/tool/PROCESSOR-SDK-AM335X

點第一個"Get Software"

(Linux Processor SDK for AM335x)

![](install_sdk.png/install_sdk_01.png)

右鍵複製連結網址"ti-processor-sdk-linux-am335x-evm-06.01.00.08-Linux-x86-Install.bin"

![](install_sdk.png/install_sdk_02.png)
![](install_sdk.png/install_sdk_03.png)

切換至Downloads資料夾

顯示所有檔案

```bash
$ cd Downloads/

$ ll
```

自動下載網頁檔案


```bash
$ wget http://software-dl.ti.com/processor-sdk-linux/esd/AM335X/latest/exports/ti-processor-sdk-linux-am335x-evm-06.01.00.08-Linux-x86-Install.bin
```

![](install_sdk.png/install_sdk_06.png)

下載中...

![](install_sdk.png/install_sdk_07.png)

顯示所有檔案

```bash
$ ll
```

![](install_sdk.png/install_sdk_08.png)

改變檔案權限，將檔案變為可執行檔

```bash
$ chmod +x ti-processor-sdk-linux-am335x-evm-06.01.00.08-Linux-x86-Install.bin
```

顯示所有檔案

```bash
$ ll
```
檔案變綠色，代表可執行

執行執行檔

```bash
$ ./ti-processor-sdk-linux-am335x-evm-06.01.00.08-Linux-x86-Install.bin
```

![](install_sdk.png/install_sdk_09.png)

Next

![](install_sdk.png/install_sdk_10.png)

Next

![](install_sdk.png/install_sdk_11.png)

Next

![](install_sdk.png/install_sdk_12.png)

Next

![](install_sdk.png/install_sdk_13.png)

Next

![](install_sdk.png/install_sdk_14.png)

Next

![](install_sdk.png/install_sdk_15.png)

安裝中...

![](install_sdk.png/install_sdk_16.png)

Finish

![](install_sdk.png/install_sdk_17.png)

顯示所有檔案

```bash
$ ll
```

![](install_sdk.png/install_sdk_18.png)



![](install_sdk.png/install_sdk_19.png)



![](install_sdk.png/install_sdk_20.png)



![](install_sdk.png/install_sdk_21.png)

Enter鍵

![](install_sdk.png/install_sdk_22.png)

y

![](install_sdk.png/install_sdk_23.png)

y

![](install_sdk.png/install_sdk_24.png)



![](install_sdk.png/install_sdk_25.png)

Enter鍵

![](install_sdk.png/install_sdk_26.png)

輸入[1]

![](install_sdk.png/install_sdk_27.png)

Enter鍵

![](install_sdk.png/install_sdk_28.png)

Enter鍵

![](install_sdk.png/install_sdk_29.png)

Enter鍵

![](install_sdk.png/install_sdk_30.png)

Enter鍵

![](install_sdk.png/install_sdk_31.png)

/dev/ttyUSB0 > Enter鍵

![](install_sdk.png/install_sdk_32.png)

192.168.50.254 > Enter鍵

![](install_sdk.png/install_sdk_33.png)

Enter鍵

![](install_sdk.png/install_sdk_34.png)

Enter鍵

![](install_sdk.png/install_sdk_35.png)

輸入n > Enter鍵

![](install_sdk.png/install_sdk_36.png)

輸入n > Enter鍵

![](install_sdk.png/install_sdk_37.png)



![](install_sdk.png/install_sdk_38.png)

```bash
$ sudo apt install bison flex libssl-dev
```

![](install_sdk.png/install_sdk_39.png)

輸入y > Enter鍵

![](install_sdk.png/install_sdk_40.png)



![](install_sdk.png/install_sdk_41.png)



![](install_sdk.png/install_sdk_42.png)



![](install_sdk.png/install_sdk_43.png)
